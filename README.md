# Backpack

A library for data storage and file packing. Not the fasting but very simple.

# Example

```
#include <backpack.h>

int main()
{
  BACKPACK* bp = create_backpack();
  free_backpack(bp);
  return 0
}
```
